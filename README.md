# CERN Hub

This repo contains the deployment and configuration of the CERN Hub,
including its multiple environments - staging, production, etc. It relies
on Helm and Flux for this purpose.

## Pre-Requisites

First get a Kubernetes cluster up and running, [check here](https://clouddocs.web.cern.ch/containers/quickstart.html).

The non-production setups rely on a CephFS share for the database storage, you
will need to [ask for some quota](https://clouddocs.web.cern.ch/file_shares/quickstart.html#0-obtain-quota-for-cephfs-shares)
on the *geneva-cephfs-testing* cluster of Manila as well.

## Deployment

First step is to [deploy Helm](https://clouddocs.web.cern.ch/containers/tutorials/helm.html).

Next deploy the secrets - see the *secrets* section below.

Finally deploy Flux and point it to this repository (the git.url value below):
```bash
$ kubectl create -f flux-helm-release-crd.yaml

$ cat flux-values.yaml
helmOperator:
  create: true
  chartsSyncInterval: 1m
  configureRepositories:
    enable: true
    repositories:
    - name: stable
      url: https://kubernetes-charts.storage.googleapis.com
    - name: jupyterhub
      url: https://jupyterhub.github.io/helm-chart/
git:
  path: releases,namespaces
  pollInterval: 1m
  readonly: true
rbac:
  create: true

$ helm install fluxcd/flux --namespace flux --name flux --values flux-values.yaml \
    --set git.url=https://gitlab.cern.ch/helm/releases/hub
```

Check the logs to see the multiple releases are being picked up:
```
$ kubectl -n flux logs -f deployment.apps/flux-helm-operator
```

All going well you should see them in helm:
```bash
$ helm ls
NAME    REVISION    UPDATED                     STATUS      CHART       APP VERSION NAMESPACE
flux    1           Tue Oct 29 13:49:30 2019    DEPLOYED    flux-0.15.0 1.15.0      flux     
hub     1           Tue Oct 29 13:50:49 2019    DEPLOYED    hub-0.1.0               prod     
hub-stg 1           Tue Oct 29 13:51:00 2019    DEPLOYED    hub-0.1.0               stg      
```

## Structure and Releases

The structure of this repo is as follows:
* **chart**: the umbrella chart, with defaults and extra setup required in addition to Binder
* **releases**: the different deployments of the Hub (staging, production),
each with overrides or complements of the default values. These are defined as
Flux HelmRelease resources

## Secrets

Secrets are stored along the release values, but encrypted using the [helm
secrets](https://gitlab.cern.ch/helm/plugins/barbican) plugin - this makes them
also version controlled.

To achieve this it means that for the moment they are not managed by Flux, a
better integration will appear later.

You need to do the following once before installing flux:
```bash
kubectl -n hub create -f releases/prod/hub-secrets.yaml
kubectl -n hub-stg create -f releases/stg/hub-secrets.yaml
```

and update them manually everytime when you update secrets.

## Updating a Release

If it is a change for all releases, change it in *chart*.

If it is a release specific change (staging, production), change it in the corresponding release directory.

Flux will detect the change and apply it locally on the cluster - if you don't
want to wait for *git.pollInterval* you can trigger a sync in the cluster:
```bash
$ export FLUX_FORWARD_NAMESPACE=flux
$ fluxctl sync
```
